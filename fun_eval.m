function f_out = fun_eval(B,x,y,SNR_mat,N_sub)
    
    if N_sub==52 % 20 MHz
        N_data=56;
        Nfft = 64;
    else         % 40 MHz
        N_data=108;
        Nfft =114;
    end
    
% Find Effective SNR for B
    SNR_eff=-B*log((sum(exp(-SNR_mat(:,1:N_sub)'/B)))/N_sub);
    ESNR= 10*log10(SNR_eff)+10*log10(Nfft/N_data); % account for null subcarriers
    per = SNR_mat(:,end)';
   
    Jec=[ESNR;per]';
    
% Remove complex values if any    
    Mat_combine=Jec(Jec~=real(Jec));
    if ~isempty(Mat_combine)
        for cmp=1:length(Mat_combine)
            index(cmp) = find(Jec==Mat_combine(cmp),1);
        end
        Jec(index,:)=[];
    end
% Sort the matrix    
    Sorted_mat=sortrows(Jec,1);
% remove values going to infinity if any    
    ind_neginf=find(Sorted_mat(:,1)==-Inf);
    if ~isempty(ind_neginf)
        Sorted_mat=Sorted_mat(ind_neginf(end)+1:end,:);
    end
    ind_inf=find(Sorted_mat(:,1)==Inf);
    if ~isempty(ind_inf)
        Sorted_mat=Sorted_mat(1:ind_inf(1)-1,:);
        max_val=ceil(Sorted_mat(ind_inf(1)-1,1));
    else
        max_val=ceil(Sorted_mat(end-1,1));   
    end

%   Quantize in bins of 0.5 dB
    bin_size=0.5;
    PER=[];
    ax=[];
    for i = floor(Sorted_mat(1,1)):bin_size:max_val
        len=0;
        per_in=0;
        for j=1:length(Sorted_mat)
            if  Sorted_mat(j,1)>i && Sorted_mat(j,1)<i+bin_size        
            len=len+1;
            per_in=per_in+Sorted_mat(j,2);
            end
        end
        if len~=0
            PER=[PER, per_in/len]; 
            ax=[ax i+bin_size/2];
        end
    end
    % take only interesting points
    ind_1=find(PER==1);
    if ind_1
        PER=PER(ind_1(end):end);
        ax=ax(ind_1(end):end);
    end
    zeros_chk=find(PER==0);
    ind_0=find(PER<0.00009);
    
    if zeros_chk
        PER=PER(1:ind_0(1)-1);
        ax=ax(1:ind_0(1)-1);
    end
  % interpolate AWGN table for above PERs
     query = PER(1:sum(PER>= y(end)));
     ax = ax(1:length(query));
     x_aw_inp=interp1(y,x,query,'pchip');
  % Return MSE   
     f_out=immse(x_aw_inp,ax);
end