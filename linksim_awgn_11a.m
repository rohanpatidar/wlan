clc
clear
close all
for mcs=0:7 
    
nonHT = wlanNonHTConfig;          % 11a configuration
nonHT.Modulation = 'OFDM';        % OFDM configuration
nonHT.ChannelBandwidth = 'CBW20'; % 20 MHz channel bandwidth
nonHT.PSDULength = 1458;          % PSDU length in bytes
nonHT.MCS = mcs;                   

x=0.2;
snrbox = [-2:x:5; 0:X:7; 1:x:8; 4:x:11; 7:x:14; 10:x:17; 13:x:20; 14:x:21];

snr=snrbox(mcs+1,:);
maxNumPEs = 400;       % The maximum number of packet errors at an SNR point
maxNumPackets = 40000; % Maximum number of packets at an SNR point

[nonHThtData,nonHThtPilots] = helperSubcarrierIndices(nonHT,'Legacy'); % nonht data field indices
Nst_ht = numel(nonHThtData)+numel(nonHThtPilots); % number of nonht data subcarriers
Nfft = helperFFTLength(nonHT);                    % FFT length

ind = wlanFieldIndices(nonHT); % noht preamble field indices
        
S = numel(snr);
packetErrorRate = zeros(S,1);

    parfor i = 1:S % Use 'parfor' to speed up the simulation
        AWGN = comm.AWGNChannel;
        AWGN.NoiseMethod = 'Signal to noise ratio (SNR)';
        AWGN.SNR = snr(i)-10*log10(Nfft/Nst_ht); % Account for energy in nulls

        % Loop to simulate multiple packets
        numPacketErrors = 0;
        n = 1; % Index of packet transmitted
        while numPacketErrors<=maxNumPEs && n<=maxNumPackets
            % Generate a packet waveform
            txPSDU = randi([0 1],nonHT.PSDULength*8,1); % Random bits
            tx = wlanWaveformGenerator(txPSDU,nonHT);   % transmitter output

            % Add noise
            rx = step(AWGN,tx);                         % add awgn noise


            % Estimate noise power in HT fields
            lltf = rx(ind.LLTF(1):ind.LLTF(2),:);
            demodLLTF = wlanLLTFDemodulate(lltf,nonHT.ChannelBandwidth);
            nVarHT = 1/(10^((snr(i)-10*log10(Nfft/Nst_ht))/10));

            chanEst = ones(52,1);   % ideal channel estimate
            nonhtdataq = rx(ind.NonHTData(1):ind.NonHTData(2),:);
            cfreg= wlanRecoveryConfig('PilotPhaseTracking','None');
            rxPSDU = wlanNonHTDataRecover(nonhtdataq,chanEst,nVarHT,nonHT,cfreg); % signal recovery 
            
            packetError = any(biterr(txPSDU,rxPSDU)); 
            numPacketErrors = numPacketErrors+packetError;
            n = n+1;
        end
        packetErrorRate(i) = numPacketErrors/(n-1);
    end

    fname = sprintf('1458_11a_mcs%d.mat',nonHT.MCS); %store per vs snr
    save(fname,'snr','packetErrorRate');
end