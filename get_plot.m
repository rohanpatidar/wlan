function [fmin,x_new,query]= get_plot(B,x,y,Mat_snrs,N_sub)
    
    if N_sub==52 % 20 MHz
        N_data=56;
        Nfft = 64;
    else         % 40 MHz
        N_data=108;
        Nfft =114;
    end
    % Find Effective SNR for B
    SNR_eff=-B*log((sum(exp(-Mat_snrs(:,1:N_sub)'/B)))/N_sub);
    ESNR= 10*log10(SNR_eff)+10*log10(Nfft/N_data); % account for null subcarriers
    per = Mat_snrs(:,end)';
   
    lenvec=[];  
    Jec=[ESNR;per]';
    aa=Jec(Jec~=real(Jec));
    if ~isempty(aa)
        for cmp=1:length(aa);
            index(cmp) = find(Jec==aa(cmp),1);
        end
        Jec(index,:)=[];
    end
    
    sorted_mat=sortrows(Jec,1);
    
    ind_neginf=find(sorted_mat(:,1)==-Inf);
    if ~isempty(ind_neginf)
        sorted_mat=sorted_mat(ind_neginf(end)+1:end,:);
    end
    ind_inf=find(sorted_mat(:,1)==Inf);
    if ~isempty(ind_inf)
        sorted_mat=sorted_mat(1:ind_inf(1)-1,:);
        max_val=ceil(sorted_mat(ind_inf(1)-1,1));
    else
        max_val=ceil(sorted_mat(end-1,1));
    end

    ss=0.5;
    PER=[];
    ax=[];
    for i = floor(sorted_mat(1,1)):ss:max_val
        len=0;
        per_in=0;
        for j=1:length(sorted_mat)
            if  sorted_mat(j,1)>i && sorted_mat(j,1)<i+ss        
            len=len+1;
            per_in=per_in+sorted_mat(j,2);
            end
        end
        lenvec=[lenvec,len];
        PER=[PER, per_in/len]; 
        ax=[ax i+ss/2];
    end

    ind_1=find(PER==1);
    if ind_1
        PER=PER(ind_1(end):end);
        ax=ax(ind_1(end):end);
        lenvec=lenvec(ind_1(end):end);
    end
    zeros_chk=find(PER==0);
    ind_0=find(PER<0.00009);
    
    if zeros_chk
        PER=PER(1:ind_0(1)-1);
        ax=ax(1:ind_0(1)-1);
        lenvec=lenvec(1:ind_0(1)-1);
    end
 
    % find N for each prate as per 0.1, if N>lenvec, then find errorbar
    % using lenvec.
    y_er=1.96; % For 95% probability, (read from Q function table)
    query = PER(1:sum(PER>= y(end)));
 
    % to find error bar 
    for k=1:length(query)
        err(k)=(y_er*sqrt(1-query(k)))/(sqrt(query(k)*lenvec(k)));
    end

    ax = ax(1:length(query));
    errorbar(ax(1:end),(query(1:end)),(err(1:end).*query(1:end)));
    set(gca,'yscale','log');
    legend('EESM')
     
     x_aw_inp=interp1(y,x,query,'pchip');
     fmin=immse(x_aw_inp(2:end),ax(2:end));
     x_new=interp1(y,x,query,'pchip');
     
end