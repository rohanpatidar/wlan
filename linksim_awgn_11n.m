% Simulated AWGN results
clear all
clc
close all

for mcs=0:7 % 8 mcs (modulation and coding scheme)
% 11n HT Configuration object 
cfgHT = wlanHTConfig;
cfgHT.ChannelBandwidth = 'CBW20'; % 20 MHz channel bandwidth
cfgHT.NumTransmitAntennas = 1;    % 1 transmit antennas
cfgHT.NumSpaceTimeStreams = 1;    % 1 space-time streams
cfgHT.PSDULength = 1458;          % PSDU length in bytes
cfgHT.MCS = mcs;                  % http://mcsindex.com/
cfgHT.RecommendSmoothing=false;

%% Simulation Parameters
x=0.2; % SNR resolution
snrbox=[-1:x:5; 2:x:8; 4:x:10; 7:x:13; 11:x:17; 15:x:21; 16:x:22; 17:x:23]; % for each mcs
% snrbox=[-3:x:4; 0:x:7; 3:x:10; 5:x:12 ; 8.5:x:15.5; 12:x:19; 13.4:x:20.4; 15:x:22]; % 32 bytes
snr = snrbox(mcs+1,:);
maxNumPEs = 400; % The maximum number of allowed packet errors at an SNR point
maxNumPackets =40000; % Max packets at an SNR point

% Get the HT fields and FFT length
[htData,htPilots] = helperSubcarrierIndices(cfgHT,'HT'); % 
Nst_ht = numel(htData)+numel(htPilots); % number of non-data subcarriers
Nfft = helperFFTLength(cfgHT); % FFT length

% Indices for accessing preamble and data field in time-domain packet
ind = wlanFieldIndices(cfgHT);        
S = numel(snr);
packetErrorRate = zeros(S,1);
parfor i = 1:S %'parfor' for parallel simulation
    % Generate AWGN object for desired SNR 
    AWGN = comm.AWGNChannel;
    AWGN.NoiseMethod = 'Signal to noise ratio (SNR)';
    AWGN.SignalPower = 1; % Normalization
    AWGN.SNR = snr(i)-10*log10(Nfft/Nst_ht); % Account for energy in nulls subcarriers
    nv=1/(10^((snr(i)-10*log10(Nfft/Nst_ht))/10)); % noise variance known at receiver
   
    numPacketErrors = 0;
    n = 1;

    while numPacketErrors<=maxNumPEs && n<=maxNumPackets
        txPSDU = randi([0 1],cfgHT.PSDULength*8,1); %generate random bits
        tx = wlanWaveformGenerator(txPSDU,cfgHT);   % Creates transmit waveform
        rx = step(AWGN,tx); 

        chanEst=ones(56,1);                                        % Ideal channel estimate 
        cfreg= wlanRecoveryConfig('PilotPhaseTracking','None');    % Turn pilot phase tracking off
        htdata = rx(ind.HTData(1):ind.HTData(2),:);                % HT data 
        rxPSDU = wlanHTDataRecover(htdata,chanEst,nv,cfgHT,cfreg); % Performs Soft viterbi decoding data recovery      
        packetError = any(biterr(txPSDU,rxPSDU));                  % check packet error
        numPacketErrors = numPacketErrors+packetError;             % increase error counter
        n = n+1;
    end    
        packetErrorRate(i) = numPacketErrors/(n-1);                % find PER
end

fname = sprintf('1458_mcs%d.mat',cfgHT.MCS );                      % Store SNR and PER
save(fname,'snr','packetErrorRate');
end
