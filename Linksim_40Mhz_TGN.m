close all
clear
clc

for mcs=0:7

% Format comfiguration for High throughput 11n
cfgHT = wlanHTConfig;
cfgHT.ChannelBandwidth = 'CBW40'; % 40 MHz channel bandwidth
cfgHT.NumTransmitAntennas = 1;    % 1 transmit antennas
cfgHT.NumSpaceTimeStreams = 1;    % 1 space-time streams
cfgHT.PSDULength = 1000;          % PSDU length in bytes
cfgHT.MCS = mcs;                   


% Create and configure the TGN channel
tgn = wlanTGnChannel; 
tgn.DelayProfile = 'Model-D';  %choose delay profile from A,B,C,D,E and F
tgn.NumTransmitAntennas = 1;
tgn.NumReceiveAntennas = 1;
tgn.TransmitReceiveDistance = 10;       % for NLOS, breakpoint distance = 10 m (channel model D)
tgn.LargeScaleFadingEffect = 'None';    % Shadowing and pathloss effects are not considered
tgn.FluorescentEffect = 0;              % No Fluoresence light effect cosidered
tgn.NormalizeChannelOutputs=0;          % Normalize output with number of receiver (not considered)  


%% Simulation Parameters
snrmat=[4:9,4:9; 6:11,6:11;  12:17,12:17;   12:17,12:17;   15:20,15:20;  18:23,18:23;   21:26,21:26;  22:27,22:27];
% SNR choosen for each MCS, PER down to 10 ^-2 essential for EESM accuracy   
snr=snrmat(mcs+1,:);
maxNumPackets = 40000; % Maximum number of packets at an SNR point

%% Simulation Setup
fs = helperSampleRate(cfgHT);
tgn.SampleRate = fs; %set channel sampling rate = input sampling rate

% Get the number of occupied subcarriers in HT fields
[htData,htPilots] = helperSubcarrierIndices(cfgHT,'HT');
Nst_ht = numel(htData)+numel(htPilots); % (data subcarriers + pilot subcarriers) 
Nfft = helperFFTLength(cfgHT);          % FFT length

% Time domain indices for fields 
ind = wlanFieldIndices(cfgHT);

S = numel(snr);
Subcarrier_mat=zeros(maxNumPackets,108+1,S); 

for i = 1:S   % Parallel simulation
    
    AWGN = comm.AWGNChannel;
    AWGN.NoiseMethod = 'Signal to noise ratio (SNR)';
    AWGN.SNR = snr(i)-10*log10(Nfft/Nst_ht); % Account for energy in nulls
    nv=1/(10^((snr(i)-10*log10(Nfft/Nst_ht))/10));
    
    n = 1; % Index of packet transmitted
    sub_snr=zeros(maxNumPackets,108+1); % 108 subcarriers and decoding decision flag
    while n<=maxNumPackets % Loop to simulate multiple packets
        % Generate a packet waveform

        txPSDU = randi([0 1],cfgHT.PSDULength*8,1); % Random bits generation
        tx = wlanWaveformGenerator(txPSDU,cfgHT);   % transmitter output waveform
                
        % Add trailing zeros to allow for channel filter delay
        tx = [tx; zeros(18,cfgHT.NumTransmitAntennas)]; % channel-D (18 paths) 
 
        %% Receiver end  
        rxx = step(tgn,tx);     % pass through multipath channel
              
        reset(tgn);             % reset channel filter state
        
        rx = step(AWGN,rxx);    % Add noise  
                             
        % Extract the Non-HT fields and determine start of L-LTF
        nonhtfields = rx(ind.LSTF(1):ind.LSIG(2),:); %    legacy field
        lltfIdx = 326; % starting time domain index for legacy long training field (multipath channel)
        
        pktOffset = lltfIdx-double(ind.LLTF(1)); 
        rx = rx(1+pktOffset:end,:);                           % offset removal

        htltf_ideal = rxx(ind.HTLTF(1)+pktOffset:ind.HTLTF(2)+pktOffset,:);
        htltfDemod = wlanHTLTFDemodulate(htltf_ideal,cfgHT);  % Finds FFT values
        chanEst = wlanHTLTFChannelEstimate(htltfDemod,cfgHT); % Ideal channel Estimate using HT-LTF signal
        
        cfgr = wlanRecoveryConfig('PilotPhaseTracking','None');             % object to turn pilot phase tracking off
        htdata = rx(ind.HTData(1):ind.HTData(2),:);                         % PSDU received data 
        [rxPSDU, sigval] = wlanHTDataRecover(htdata,chanEst,nv,cfgHT,cfgr); % soft viterbi ,data recover 
        %% find snr per subcarrier
        htpilots= [6 34 48 67 81 109]; 
        chanEst(htpilots)=[];                                  % remove 4 pilots from 56 data carriers
        netsnr = ((abs(chanEst)).^2)/nv;                       % find subcarrier snrs                                       
        packetError = any(biterr(txPSDU,rxPSDU));              % Decoding result
        sub_snr(n,:)=[netsnr',packetError];                             
        n = n+1;
    end
        Subcarrier_mat(:,:,i)=sub_snr;
end

fname = sprintf('40Mhz_D_mcs%d.mat',cfgHT.MCS );
save(fname,'Subcarrier_mat');
end