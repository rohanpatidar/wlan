# README #

Matlab Wlan 802.11n link simulation codes for ns-3. 
There are 4 different codes:
 
1) AWGN simulation

2) Multipath frequency selective channel simulation channel model D and E (20 and 40 MHz).

3) Code for frequency selective channel that stores sub carrier SNRs, stored data (.mat) is further utilized and necessary for obtaining optimal tuning parameter for link to system mapping technique: EESM (code included)

4) In order to validate, script to generate channel realizations is included along with code to validate EESM for optimal Beta for the above realizations. 

5) For testing, AWGN table for 802.11n is included which is necessary for EESM implementation. (It can be simulated too by following the code in AWGN Simulation Model)
  
6) For corresponding ns-3 implementation of tables based on this simulator, please see the 'error-table' branch of

https://tomhend@bitbucket.org/tomhend/ns-3-error-model.git

In order to run the codes, it is required to have MATLAB WLAN System Toolbox installed. https://www.mathworks.com/products/wlan-system.html

The changes are made to a sample Wlan toolbox example to obtain desired link simulation results.

* Contact: rpatidar@uw.edu