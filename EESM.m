% This script loads the SNR and decoding result for any mcs and finds best
% tuning parameter by minimising the MSE function for EESM (Exponential Effective
% SNR Mapping method). For minimization, Nelder-mead algortihm is employed.  

clear
clc
close all
%load AWGN results for 1458 bytes using linksim_11n
% and interpolate to get PER table for 1000 bytes
load('1458_mcs2.mat')  % provide AWGN table (2 vectors PER and SNR)
per_1000=1-(1-packetErrorRate).^(1000/1458); % 
indxb=per_1000>0.00005; % consider only per down to 0.00005
ind_1=find(per_1000==1); 
ind_end=find(indxb==1);
y=per_1000(ind_1(end):ind_end(end))'; % find the interesting snrs and per 
x=snr(ind_1(end):ind_end(end))'; 

load('40Mhz_D_mcs0.mat') %load the matrix of subcarrier SNRs and decoding results
snr_len=length(Subcarrier_mat(1,1,:));
packets_num=length(Subcarrier_mat(:,1,1));
num_subcarriers=length(Subcarrier_mat(1,:,1))-1;
SNR_mat=zeros(snr_len*packets_num,num_subcarriers+1);
for j=1:snr_len
    sub_SNRs=Subcarrier_mat(:,:,j);
    SNR_mat(packets_num*(j-1)+1:packets_num*(j),:)=sub_SNRs;
end

fun = @(B)fun_eval(B,x,y,SNR_mat,num_subcarriers); % minimizing funciton holder (EESM calcuation and MSE)

B_in=1;  % initialize Beta parameter
Bx=fminsearch_new(fun,B_in); % applies Nelder-mead method to find optimal parameter 


% plot curves EESM and AWGN
[fmin,x,y]= get_plot(Bx,x,y,SNR_mat,num_subcarriers); 
hold on 
%% AWGN error bar and plot
indxb2=y>=0.00005;
ind_12=find(y==1);
ind_end2=find(indxb2==1);
y=y(ind_12(end):ind_end2(end))'; 
x=x(ind_12(end):ind_end2(end))';

y_er=1.96;
for k=1:length(y)
    if y(k)>0.01
        N(k)=round(400/y(k));
    else
        N(k)=40000;
    end
end

for k=1:length(y)
    errq(k)=(y_er*sqrt(1-y(k)))/(sqrt(y(k)*N(k))); % finding error bars
end

errorbar(x(1:end),(y(1:end)),(errq(1:end).*y(1:end)'));
set(gca,'yscale','log');
legend('EESM','AWGN')
grid on
title('EESM Mapping of Channel D MCS-1')
xlabel('Effecive SNR(dB)')
ylabel('PER')